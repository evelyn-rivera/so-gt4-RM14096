#include <stdio.h>
#include <stdlib.h>

void maximo(float *datos, int tam);
void minimo(float *dato, int tamn);
void media(float *datos, int tam);

int main()
{
    int tamanio;
    do
    {
        printf("Ingrese la cantidad de numeros para la operacion:");
        scanf("%d", &tamanio);
    } while (tamanio <= 0);

    float *numeros = (float *)malloc(tamanio * sizeof(float));

    for (int i = 0; i < tamanio; i++)
    {
        printf("Ingrese el dato #%d:", (i + 1));
        scanf("%f", numeros);
        numeros++;
    }
    numeros--;
    maximo(numeros, tamanio);
    minimo(numeros, tamanio);
    media(numeros, tamanio);
}

void maximo(float *datos, int tam)
{
    float max = *datos;
    for (int j = 0; j < tam; j++)
    {
        if (max < *datos)
        {
            max = *datos;
        }
        datos--;
    }
    printf("El numero maximo es: %f\n", max);
    return;
}

void minimo(float *dato, int tamn)
{
    float min = *dato;
    for (int j = 0; j < tamn; j++)
    {
        if (min > *dato)
        {
            min = *dato;
        }
        dato--;
    }
    printf("El numero minimo es: %f\n", min);
}

void media(float *datos, int tam)
{
    float suma = 0;
    for (int j = 0; j < tam; j++)
    {
        suma = suma + *datos;
        datos--;
    }
    float media = suma / tam;
    printf("La media de los datos es: %f\n", media);
}